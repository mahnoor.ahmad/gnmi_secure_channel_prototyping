import gnmi.proto
import grpclib.client
import ssl

def create_ssl_context():
    # Create an SSL context
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS)
    ssl_context.load_cert_chain(certfile="client.pem", keyfile="client-key.pem")
    ssl_context.load_verify_locations(cafile="rampupCA.pem")
    return ssl_context

async def main():
    ssl_context = ssl.create_default_context(cafile="rampupCA.pem")
    ssl_context = create_ssl_context()
    channel = grpclib.client.Channel(host="127.0.0.1", port=8180, ssl=ssl_context)
    service = gnmi.proto.gNMIStub(
        channel, metadata={"username": "admin", "password": "secret"}
    )

    response = await service.capabilities()
    print(response.to_json(indent=2))

    response = await service.get(
        path=[gnmi.proto.Path(elem=[gnmi.proto.PathElem(name="system")])],
    )
    print(response.to_json(indent=2))


if __name__ == "__main__":
    import asyncio
    asyncio.run(main())
