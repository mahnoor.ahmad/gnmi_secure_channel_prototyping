## Small prototype client + sample GNMI target

## Server

**gNMI Target Server**


You can use the google/gnxi/gnmi_target to test the client code. You can set this up using the the following commands (assuming you have ``golangandGOPATH` configured correctly).

**Installation**

`go get -u github.com/google/gnxi/gnmi_target`

`go install -v github.com/google/gnxi/gnmi_target`


**Sample Configuration**
You can use the provided sample configuration. If you do not have a local copy of the repository, fetch it using curl.

curl -sLO https://raw.githubusercontent.com/python-gnxi/python-gnmi-proto/master/tests/integration/fixtures/config.json

*Adapted from https://github.com/python-gnxi/python-gnmi-proto/blob/main/CONTRIBUTING.md#gnmi-target-server*

**Run Server**

Once you have a configuration you can start the server like shown below.

```
gnmi_target \
    -bind_address ":8180" \
    -username admin \
    -password secret \
    -logtostderr \
    -notls \
    -config tests/integration/fixtures/config.json
```

For a TLS enabled server, I used

`gnmi_target     -bind_address ":8180"    -logtostderr -key server-key.pem -cert server.pem -ca rampupCA.pem  -config config.json -username admin -password secret -insecure`

The `-insecure` flag for the time being does not request a cert from the client, from what I have seen. Likewise, the code in `samplegnmiclient.py` is only using client-side SSL validation, not mutual TLS.

I have provided the `ca` flag because this server insists on having one once it gets its own key and cert :/ for some reason.

*Note: GRPC communication does not always indicate what the error is when there is a TLS issue. The message tends to be cryptic, about timeouts or lost connections.*

## Client

The client uses the gnmi.proto package, which is the proto codegen distributed as a Python package. To install, activate your Python env and do:

`pip install gnmi-proto`

You can check more details about the package at PyPI.

Most of the trivial code used is adapted from https://github.com/python-gnxi/python-gnmi-proto/tree/main . I have only added an SSL context to it.